> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Assignment 2 Requirements:

*Deliverables:*

1. Provide Bitbucket read-only access to a2 repo, *must* include README.md, using Markdown
syntax. (See README.md requirements above.)
2. Blackboard Links: a2 Bitbucket repo


#### README.md file should include the following items:

* Assessment items (Links to the assignments that were created on local host). 
* A screenshot of the query results from the following link: http://localhost:9999/hello/querybook.html





#### Assignment Links:

*http://localhost:9999/hello (displays directory, needs index.html)*


*http://localhost:9999/hello/HelloHome.html (Can rename "HelloHome.html" to "index.html")*:


*http://localhost:9999/hello/sayhello (invokes HelloServlet) *


*http://localhost:9999/hello/querybook.html *


*http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)*



#### Assignment Screenshots:

* Query Results for Kumar:


![Query Results:Kumar](img/3.png)


* Query Results for Ah Teck:


![Query Results:Ah Teck](img/1.png)


* Query Results for Ali:


![Query Results:Ali](img/2.png)